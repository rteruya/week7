var app = angular.module('rApp', ['ngRoute', 'ngAnimate']);

app.config(function($routeProvider, $locationProvider){
    $routeProvider
     .when('/', {
           templateUrl : 'home.html',
           controller :    'homeController'
      })
      .when('/contact', {
           templateUrl: 'contact.html',
           controller: 'contactController'
      });

    /*$locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });*/
});